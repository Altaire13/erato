import numpy as np
from matplotlib import pyplot as plt

rowtype = [('proc', int), ('all', float), ('worst', float)]
data = np.loadtxt('timelog.log', rowtype)

plt.title('Time for finding primes in $[1, 10^8]$')
plt.xlabel('Processes')
plt.ylabel('Time, sec')

plt.plot(data['proc'], data['all'], label='all')
plt.plot(data['proc'], data['worst'], label='worst')
plt.legend(loc='best')
plt.show()
