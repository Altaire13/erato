#include <iostream>
#include <vector>
#include <cstdint>
#include <cmath>
#include <algorithm>
#include <fstream>

#include <mpi/mpi.h>

int ParseArgs(int argc, char** argv, int64_t& first, int64_t& last,
              std::string* filename) {
    if (argc < 3) {
        return -1;
    }

    char* tail;

    first = strtoll(argv[1], &tail, 10);
    last = strtoll(argv[2], &tail, 10);

    if (first <= 0 || last <= 0 || first > last) {
        return -2;
    }

    if (argc > 3) {
        *filename = argv[3];
    } else {
        *filename = "";
    }

    return 0;
}

std::vector<int64_t> CalcHeadPrimes(int64_t head_max) {
    std::vector<bool> head_is_prime(head_max + 1, true);
    std::vector<int64_t> head_primes = {2};

    int64_t num;
    for (num = 3; num * num <= head_max; num += 2) {
        if (head_is_prime[num]) {
            head_primes.push_back(num);

            for (int64_t not_prime = num * num; not_prime <= head_max;
                    not_prime += 2 * num) {
                head_is_prime[not_prime] = false;
            }
        }
    }

    for (; num <= head_max; num += 2) {
        if (head_is_prime[num]) {
            head_primes.push_back(num);
        }
    }

    return head_primes;
}

std::vector<int64_t> CalcLocalPrimes(int64_t my_first, int64_t my_last,
                                     const std::vector<int64_t>& head_primes) {
    my_first = my_first + (1 - (my_first & 1));

    int64_t my_width = my_last - my_first + 1;

    int64_t odd_width = (my_width + 1) / 2;

    std::vector<bool> is_prime(odd_width, true);

    std::vector<int64_t> primes;

    if (my_first <= 1 && my_last >= 1) {
        is_prime[(1 - my_first) >> 1] = false;
    }

    if (my_first <= 2 && my_last >= 2) {
        primes.push_back(2);
    }

    size_t head_ind = 0;
    if (head_primes[head_ind] == 2) {
        ++head_ind;
    }

    for (; head_ind < head_primes.size(); ++head_ind) {
        int64_t cur_prime = head_primes[head_ind];
        int64_t not_prime =
                std::max(cur_prime * cur_prime,
                         ((my_first - 1) / cur_prime + 1) * cur_prime);

        if (not_prime % 2 == 0) {
            not_prime += cur_prime;
        }

        for (int64_t num_ind = (not_prime - my_first) >> 1;
                num_ind < odd_width; num_ind += cur_prime) {
            is_prime[num_ind] = false;
        }
    }

    for (int64_t num_ind = 0; num_ind < odd_width; ++num_ind) {
        if (is_prime[num_ind]) {
            primes.push_back(my_first + 2 * num_ind);
        }
    }

    return primes;
}

std::vector<int64_t> GatherPrimes(int rank, int proc_num,
                                  const std::vector<int64_t>& primes) {
    int prime_count = primes.size();

    if (rank == 0) {
        std::vector<int64_t> all_primes;

        std::vector<int> counts(proc_num);

        MPI_Gather(&prime_count,  1, MPI_INT,
                   counts.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);

        all_primes = std::vector<int64_t>(std::accumulate(counts.begin(),
                                                          counts.end(),
                                                          int64_t(0)));

        std::vector<int> displs(proc_num);
        displs[0] = 0;

        for (int i = 1; i < proc_num; ++i) {
            displs[i] = displs[i - 1] + counts[i - 1];
        }

        MPI_Gatherv(primes.data(), primes.size(), MPI_LONG,
                    all_primes.data(), counts.data(), displs.data(), MPI_LONG,
                    0, MPI_COMM_WORLD);

        return all_primes;
    } else {
        MPI_Gather(&prime_count, 1, MPI_INT, 0, 0, 0, 0, MPI_COMM_WORLD);
        MPI_Gatherv(primes.data(), primes.size(), MPI_LONG,
                    0, 0, 0, 0, 0, MPI_COMM_WORLD);

        return {};
    }
}

std::vector<int64_t> CalcAllPrimes(int rank, int proc_num, int64_t first,
                                   int64_t last, double* my_time = NULL) {
    int64_t width = last - first + 1;

    if (width % proc_num != 0) {
        width = (width / proc_num + 1) * proc_num;
        last = first + width - 1;
    }

    int64_t my_width = width / proc_num;

    int64_t head_max = sqrt(last);

    double start = MPI_Wtime();

    std::vector<int64_t> head_primes = CalcHeadPrimes(head_max);

    int64_t my_first = first + my_width * rank;
    int64_t my_last = my_first + my_width - 1;

    std::vector<int64_t> primes = CalcLocalPrimes(my_first, my_last,
                                                  head_primes);

    double finish = MPI_Wtime();

    std::vector<int64_t> all_primes = GatherPrimes(rank, proc_num, primes);

    if (my_time) {
        *my_time = finish - start;
    }

    return all_primes;
}

void OutputPrimes(std::vector<int64_t> primes, std::ostream& output) {
    for (size_t pi = 0; pi < primes.size(); ++pi) {
        output << primes[pi] << std::endl;
    }
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int proc_num;
    int rank;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);

    int64_t first, last;
    std::string filename;

    int err = ParseArgs(argc, argv, first, last, &filename);
    if (err) {
        if (rank == 0) {
            if (err == -1) {
                std::cerr << "Usage: " << argv[0] <<
                        " first last [output_filename]" << std::endl;
            } else if (err == -2) {
                std::cerr << "Invalid borders" << std::endl;
            }

            std::cerr << "Error parsing arguments" << std::endl;
        }

        MPI_Finalize();
        return 0;
    }

    double my_time;

    std::vector<int64_t> all_primes = CalcAllPrimes(rank, proc_num,
                                                    first, last, &my_time);

    double worst_time;
    MPI_Reduce(&my_time, &worst_time, 1, MPI_DOUBLE,
               MPI_MAX, 0, MPI_COMM_WORLD);

    double all_time;
    MPI_Reduce(&my_time, &all_time, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "All time:   " << all_time << std::endl;
        std::cout << "Worst time: " << worst_time << std::endl;

        std::ofstream timelog("timelog.log", std::ios::app);
        timelog << proc_num << " " << all_time << " " << worst_time << std::endl;

        if (filename.size()) {
            std::ofstream output;
            if (filename != "stdout") {
                output = std::ofstream(filename);
                OutputPrimes(all_primes, output);
                output.close();
            } else {
                OutputPrimes(all_primes, std::cout);
            }
        }

        std::cout << all_primes.size() << std::endl;
    }

    MPI_Finalize();

    return 0;
}

